//
//  BusinessLogic.swift
//  calc
//
//  Created by Admin on 27.08.2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import Darwin.C.math
import CoreData
import UIKit


extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext

  var SberEntityArray = [SberEntity]()
func saveToCoreData(){
    
}



func clearTable(){
    let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "SberEntity")
    let request = NSBatchDeleteRequest(fetchRequest: fetch)
    do{
        try context.execute(request)
    }catch{
        print("Error Clearing Transaction Table")
    }
}

struct inputData {
 //let credit:Double = 1182732.11
  //  let range:Double = 60
  //  let procent:Double = 11.7
// let credit:Double = 1182732.11
// let range:Double = 60
//let procent:Double = 11.7
    //34
    let credit:Double = 390000
    let range:Double = 57
    let procent:Double = 11.7
    let dataPlatesha = DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2013, month: 11, day: 3)
}



//1. Процентная ставка по кредиту в месяц = годовая процентная ставка / 12 месяцев 20%/12 месяцев/100=0,016.

//2. Коэффициент аннуитета = (0,016*(1+0,016)^36)/((1+0,016)^36—1)=0,03676.

//3. Ежемесячный аннуитетный платеж = 0,03676*1 000 000 рублей = 37 163,58 рубля.


/*
 
 E – размер ежемесячного платежа,
 
 S –  размер кредита,
 
 t – количество месяцев (или оставшихся месяцев);
 
 M – ставка годовая, согласно договору;
 
 dl– количество дней в конкретном месяце;
 
 DY – дней в году.
 
 Ежемесячный платеж по аннуитетной схеме погашения определяется так:
 
 E=S*(m*(1+m)^n)/((1+m)^n-1), где
 m – месячная процентная ставка,
 
 n – количество месяцев по договору.
 Сумма основного долга = аннуитетный платеж – проценты.
 
 Сумма процентов = остаток долга * месячную процентную ставку.
 
 Остаток основного долга = остаток предыдущего периода – сумму основного долга в предыдущем периоде.
 */

precedencegroup PowerPrecedence { higherThan: MultiplicationPrecedence }
infix operator ^^ : PowerPrecedence
func ^^ (radix: Double, power: Double) -> Double {
    return Double(pow(Double(radix), Double(power)))
}

func dosrhochnoye_pogashenie(creditInfo:inputData,summa sum:Double, at date:Date){
       print("Dosrochnoey")
//    var ostatok_dolga_po_kredity = creditInfo.credit -
    
}






func calc(table:UITableView) ->(){
    // print("test")
    var creditData = inputData()
    var k:Double = 0
    print("KREDIT \(creditData.credit)")
    var procentnay_stavka_v_month = creditData.procent / 12 / 100
    
 
    print("Procent stavka v month \(procentnay_stavka_v_month)")
    var i1 = (1+procentnay_stavka_v_month) ^^ creditData.range
   
    k = (procentnay_stavka_v_month * i1) / (i1 - 1)
 
    var plata_v_month = k * creditData.credit
    
  
    print("PLATA V MONTH \(Double(round(100*plata_v_month)/100))")
    
    
    var full_kredit = plata_v_month*creditData.range
    full_kredit = Double(round(100*full_kredit)/100)
    print("FULL KREDIT: \(full_kredit)")
    var pereplata = full_kredit - creditData.credit
    print("Pereplata: \(pereplata)")
    // print("2³ = \(2 ^^ 6)")
    
    var platesh_po_procentam =  creditData.credit * procentnay_stavka_v_month
    print("PROCENT PO PLATESHY: \(platesh_po_procentam)")
    //  print("TEST")
    
    
 //   var dosrohnoye_pogashenie_summ:Double = 250000
    
    
    
    
    
    var current_kredit = creditData.credit
    var osnovnoy_dolg:Double = 0
    
   // current_kredit -= dosrohnoye_pogashenie_summ
  //  current_kredit -= 182733
    print("CURRENT KREDIT: \(current_kredit)")
    
    
    
    
    
    print("START")
    var date_for_payment = DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2018, month: 03, day: 03)
    var  nextMonth = Calendar.current.date(from: date_for_payment)
    for index in 0..<Int(creditData.range){
         let newItem = SberEntity(context: context)
     
       
        
       
        
        newItem.payment_date = nextMonth
        osnovnoy_dolg = plata_v_month - platesh_po_procentam
       
        print(osnovnoy_dolg)
          newItem.osnovnoy_dolg = Double(osnovnoy_dolg).rounded(toPlaces: 2)
        print(newItem.osnovnoy_dolg)
        current_kredit -= osnovnoy_dolg
        print("Month: \(index+1) : \(Double(round(100*osnovnoy_dolg)/100)) :\(Double(round(100*platesh_po_procentam)/100))")
        
         newItem.procent = Double(platesh_po_procentam).rounded(toPlaces: 2)
       
        platesh_po_procentam = current_kredit * procentnay_stavka_v_month
        
        
        print("Ostatok_dolga: \(Double(round(100*current_kredit)/100))")
          newItem.ostatok_po_dolgy = Double(current_kredit).rounded(toPlaces: 2)
        print("PLATA V MONTH \(Double(round(100*plata_v_month)/100))")
         newItem.summ_for_pay = Double(plata_v_month).rounded(toPlaces: 2)
        
       
      
      
       
      
       
        SberEntityArray.append(newItem)
        nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: nextMonth!)
    }
    
    do{
        try context.save()
    }catch{
        print("Error Saving context")
    }
    DispatchQueue.main.async(){
    table.reloadData()
    }
    
    
    
    dosrhochnoye_pogashenie(creditInfo:creditData,summa:250000, at:Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2018, month: 08, day: 22))!)
 
    
}
